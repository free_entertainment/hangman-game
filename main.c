#include <stdio.h>
#include <stdlib.h>

char* rand_word(size_t);
void analyze_user_input(const char*, char*, char);
void game(const char*, size_t);
int check_win_condition(const char* secret_word, char* tmp_str);

//generate random string
char* rand_word(size_t size)
{
    char* str = malloc(size * sizeof(char));
    const char charset[] = "abcdefghijklmnopqrstuvwxyz";
    if (size) {
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }

    return str;
}

void analyze_user_input(const char* secret_word, char* tmp_str, char input)
{
  for (size_t i = 0; i < sizeof(secret_word) + 1; i++) {
    if (secret_word[i] == input)
      tmp_str[i] = input; 
  }

  return;
}

int check_win_condition(const char* secret_word, char* tmp_str)
{
  for (; *secret_word != 0; secret_word++, tmp_str++)
    if (*secret_word != *tmp_str)
      return 0;

  return 1;
}

void game(const char* secret_word, size_t sz) 
{
  char letter = 0;
  int win; // 1 win, 0 lose
  int lives = 10;
  char* tmp_str = malloc(sz * sizeof(char)); 

  for (size_t i = 0; i < sz; i++)
    tmp_str[i] = '*';
  tmp_str[sz] = '\0';
  
  //to see secret word
  //printf("tmp string = %s, secret word = %s\n", tmp_str, secret_word);

  //main loop
  while (1) {
    if (lives > 0) {
      printf("Your secret word = %s, your lives = %d \n\n", tmp_str, lives);
      printf("choose a letter = ");

      letter = fgetc(stdin);
      analyze_user_input(secret_word, tmp_str, letter); 

      if (check_win_condition(secret_word, tmp_str)) {
        win = 1;
        break;
      }

      //avoid backspace
      while (fgetc(stdin) != 0xA) {}
      lives--;  
    }
    else {
      win = 0;
      break;
    }
  }

  if (win)
    printf("\n\nYou win !!");
  else
    printf("\n\nYou lose, the word was %s", secret_word);

  return; 
}

int main(void) 
{
  char* secret_word;
  size_t word_sz = 5;
  //here i will generate a random secret_word, but you can make your own word by reading a file for example
  secret_word = rand_word(word_sz);

  game(secret_word, word_sz); 
  return 0;
}
